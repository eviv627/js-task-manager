package ru.evseenko.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class SuperDomain extends Identifiable {

    public SuperDomain() {
        super();
        name = "";
        description = "";
        status = Status.PLANNED;
    }

    @NotNull
    @Column(name="name", nullable = false)
    private String name;

    @NotNull
    @Column(name="description", nullable = false)
    private String description;

    @Nullable
    @Column(name="start_date", columnDefinition="TIMESTAMPTZ")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date startDate;

    @Nullable
    @Column(name="end_date", columnDefinition="TIMESTAMPTZ")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date endDate;

    @Nullable
    @Column(name="create_date", columnDefinition="TIMESTAMPTZ")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date createDate;

    @NotNull
    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
