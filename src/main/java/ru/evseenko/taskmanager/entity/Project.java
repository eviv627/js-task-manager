package ru.evseenko.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import org.springframework.cache.annotation.Cacheable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "app_project")
@NamedQueries(value = {
        @NamedQuery(
                name = Project.FIND_BY_USER,
                query = "select project from Project project where project.user.id = :userId"
        ),
        @NamedQuery(
                name = Project.FIND_BY_NAME,
                query = "select project from Project project where project.name like CONCAT(:name,'%') " +
                "and project.user.id = :userId"
        ),
        @NamedQuery(
                name = Project.FIND_BY_DESC,
                query = "select project from Project project where project.description like CONCAT(:description,'%') " +
                        "and project.user.id = :userId"
        )
})
public class Project extends Domain {

    public static final String FIND_BY_USER = "Project.findByUser";
    public static final String FIND_BY_NAME = "Project.findByNamePart";
    public static final String FIND_BY_DESC = "Project.findByDescriptionPart";

    @Nullable
    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Task> tasks;
}
