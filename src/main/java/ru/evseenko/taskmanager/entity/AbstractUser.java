package ru.evseenko.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUser extends Identifiable {

    public AbstractUser() {
        super();
        username = "";
        passwordHash = "";
        email = "";
        active = false;
        role = Role.USER;
    }

    @Nullable
    @Column(name="username", unique = true)
    private String username;

    @Nullable
    @Column(name="password_hash", nullable = false)
    private String passwordHash;

    @Nullable
    @Column(name="email")
    private String email;

    @Nullable
    @Column(name="role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "active", nullable = false)
    private boolean active;
}
