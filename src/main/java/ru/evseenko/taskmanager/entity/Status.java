package ru.evseenko.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public enum Status implements Comparable<Status> {

    PLANNED("PLANNED"),

    IN_PROGRESS("IN_PROGRESS"),

    DONE("DONE");

    @Getter
    @NotNull
    private String name;

    @JsonCreator
    public static Status forValue(String value) {
        return Status.valueOf(value);
    }

    @JsonValue
    public static String toValue(Status status) {
        return status.getName();
    }

    Status(@NotNull String name) {
        this.name = name;
    }

}
