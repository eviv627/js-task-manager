package ru.evseenko.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import org.springframework.cache.annotation.Cacheable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;


@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "app_task")
@NamedQueries(value = {
        @NamedQuery(
                name = Task.FIND_BY_USER,
                query = "select task from Task task where task.user.id = :userId"
        ),
        @NamedQuery(
                name = Task.FIND_BY_NAME,
                query = "select task from Task task where task.name like CONCAT(:name,'%') " +
                "and task.user.id = :userId"
        ),
        @NamedQuery(
                name = Task.FIND_BY_DESC,
                query = "select task from Task task where task.description like CONCAT(:description,'%') " +
                "and task.user.id = :userId"
        ),
        @NamedQuery(
                name = Task.FIND_BY_PROJECT,
                query = "select task from Task task where task.project.id = :projectId " +
                "and task.user.id = :userId"
        )
})
public class Task extends Domain {

    public static final String FIND_BY_USER = "Task.findByUser";
    public static final String FIND_BY_NAME = "Task.findByNamePart";
    public static final String FIND_BY_DESC = "Task.findByDescriptionPart";
    public static final String FIND_BY_PROJECT = "Task.findTaskForProject";

    @Nullable
    @JoinColumn(name = "project_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;
}
