package ru.evseenko.taskmanager.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class AuthRequest {

    AuthRequest() {
        username = "";
        password = "";
    }

    @NotNull
    private String username;
    @NotNull
    private String password;
}
