package ru.evseenko.taskmanager.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class SimpleResponse {

    public SimpleResponse() {
        success = false;
        value = "";
    }

    @NotNull
    private Boolean success;
    @NotNull
    private String value;
}
