package ru.evseenko.taskmanager.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.evseenko.taskmanager.api.entity.Identifiable;
import ru.evseenko.taskmanager.entity.AbstractUser;
import ru.evseenko.taskmanager.entity.Role;
import ru.evseenko.taskmanager.entity.User;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@Setter
@Cacheable
@Table(name = "app_user")
@XmlSeeAlso({Role.class})
@XmlRootElement(name = "user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries(value = {
        @NamedQuery(
                name = UserDTO.FIND_BY_USERNAME,
                query = "SELECT userDTO from UserDTO userDTO where userDTO.username = :username",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")}
        )
})
public class UserDTO extends AbstractUser implements Serializable, UserDetails, Identifiable {

    public static final String FIND_BY_USERNAME = "UserDTO.findUserByUsername";

    public UserDTO() {
        super();
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id='" + getId() + '\'' +
                ", login='" + getUsername() + '\'' +
                ", passwordHash='" + getPasswordHash() + '\'' +
                ", role=" + getRole() +
                '}';
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return getPasswordHash();
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(getRole());
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return isActive();
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return isActive();
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return isActive();
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return isActive();
    }
}
