package ru.evseenko.taskmanager.dto;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.Cacheable;
import ru.evseenko.taskmanager.api.entity.IDomainDTO;
import ru.evseenko.taskmanager.api.entity.Identifiable;
import ru.evseenko.taskmanager.entity.Status;
import ru.evseenko.taskmanager.util.DateFormat;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;

@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_project")
@XmlSeeAlso({Status.class})
@XmlRootElement(name = "project")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries(value = {
        @NamedQuery(
                name = ProjectDTO.FIND_BY_USER,
                query = "select projectDTO from ProjectDTO projectDTO where projectDTO.userId = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        ),
        @NamedQuery(
                name = ProjectDTO.FIND_BY_NAME,
                query = "select projectDTO from ProjectDTO projectDTO where projectDTO.name like CONCAT(:name,'%') " +
                        "and projectDTO.userId = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        ),
        @NamedQuery(
                name = ProjectDTO.FIND_BY_DESC,
                query = "select projectDTO from ProjectDTO projectDTO where projectDTO.description like CONCAT(:description,'%') " +
                        "and projectDTO.userId = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        )
})
public class ProjectDTO extends DomainDTO implements Serializable, IDomainDTO, Identifiable {

    public static final String FIND_BY_USER = "ProjectDTO.findByUser";
    public static final String FIND_BY_NAME = "ProjectDTO.findByNamePart";
    public static final String FIND_BY_DESC = "ProjectDTO.findByDescriptionPart";

    @Override
    public String toString() {
        return "ProjectDTO {" +
                "id='" + getId() + '\'' +
                ", name='" + getName() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", startDate=" + (getStartDate() != null ? DateFormat.format(getStartDate()) : "") +
                ", endDate=" + (getEndDate() != null ? DateFormat.format(getEndDate()) : "") +
                ", createDate=" + (getCreateDate() != null ? DateFormat.format(getCreateDate()) : "") +
                ", status=" + getStatus() +
                ", userId='" + getUserId() + '\'' +
                '}';
    }
}
