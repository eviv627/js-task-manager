package ru.evseenko.taskmanager.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.taskmanager.entity.SuperDomain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class DomainDTO extends SuperDomain {

    public DomainDTO() {
        super();
        userId = "";
    }

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

}
