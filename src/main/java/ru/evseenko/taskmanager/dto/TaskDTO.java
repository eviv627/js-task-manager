package ru.evseenko.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cache.annotation.Cacheable;
import ru.evseenko.taskmanager.api.entity.IDomainDTO;
import ru.evseenko.taskmanager.api.entity.Identifiable;
import ru.evseenko.taskmanager.entity.Status;
import ru.evseenko.taskmanager.entity.Task;
import ru.evseenko.taskmanager.util.DateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;

@Entity
@Setter
@Getter
@Cacheable
@Table(name = "app_task")
@XmlSeeAlso({Status.class})
@XmlRootElement(name = "task")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries(value = {
        @NamedQuery(
                name = TaskDTO.FIND_BY_USER,
                query = "select taskDTO from TaskDTO taskDTO where taskDTO.userId = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }

        ),
        @NamedQuery(
                name = TaskDTO.FIND_BY_NAME,
                query = "select taskDTO from TaskDTO taskDTO where taskDTO.name like CONCAT(:name,'%') " +
                        "and taskDTO.userId = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        ),
        @NamedQuery(
                name = TaskDTO.FIND_BY_DESC,
                query = "select taskDTO from TaskDTO taskDTO where taskDTO.description like CONCAT(:description,'%') " +
                        "and taskDTO.userId = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        ),
        @NamedQuery(
                name = TaskDTO.FIND_BY_PROJECT,
                query = "select taskDTO from TaskDTO taskDTO where taskDTO.projectId = :projectId " +
                        "and taskDTO.userId = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        )
})
public class TaskDTO extends DomainDTO implements Serializable, IDomainDTO, Identifiable {

    public static final String FIND_BY_USER = "TaskDTO.findByUser";
    public static final String FIND_BY_NAME = "TaskDTO.findByNamePart";
    public static final String FIND_BY_DESC = "TaskDTO.findByDescriptionPart";
    public static final String FIND_BY_PROJECT = "TaskDTO.findTaskForProject";

    public TaskDTO() {
        super();
        projectId = null;
    }

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @Override
    public String toString() {
        return "TaskDTO {" +
                "id='" + getId() + '\'' +
                ", name='" + getName() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", startDate=" + (getStartDate() != null ? DateFormat.format(getStartDate()) : "") +
                ", endDate=" + (getEndDate() != null ? DateFormat.format(getEndDate()) : "") +
                ", createDate=" + (getCreateDate() != null ? DateFormat.format(getCreateDate()) : "") +
                ", status=" + getStatus() +
                ", userId='" + getUserId() + '\'' +
                ", projectId='" + projectId + '\'' +
                '}';
    }
}
