package ru.evseenko.taskmanager.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class RegistrationRequest {

    RegistrationRequest() {
        username = "";
        password = "";
        passwordConfirm = "";
        email = "";
    }

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String passwordConfirm;

    @NotNull
    private String email;

}
