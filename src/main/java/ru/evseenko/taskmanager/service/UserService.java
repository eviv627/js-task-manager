package ru.evseenko.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.evseenko.taskmanager.api.repository.IUserDTORepository;
import ru.evseenko.taskmanager.api.repository.IUserRepository;
import ru.evseenko.taskmanager.api.service.IUserService;
import ru.evseenko.taskmanager.dto.UserDTO;
import ru.evseenko.taskmanager.entity.User;
import ru.evseenko.taskmanager.exception.ServiceException;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements IUserService, UserDetailsService {

    private final IUserRepository userRepository;

    private final IUserDTORepository userDTORepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(
            IUserRepository userRepository,
            IUserDTORepository userDTORepository
    ) {
        this.userRepository = userRepository;
        this.userDTORepository = userDTORepository;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO loadUserByUsername(@NotNull final String username) {
        return userDTORepository.findUserByUsername(username);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO get(@NotNull final String id) {
        @NotNull final Optional<UserDTO> optional = userDTORepository.findById(id);
        if(!optional.isPresent()) {
            throw new ServiceException("Incorrect id");
        }
        return optional.get();
    }

    @NotNull
    @Override
    @Transactional
    public Boolean update(@NotNull final UserDTO entityDTO) {
        @NotNull final Optional<UserDTO> optional = userDTORepository.findById(entityDTO.getId());
        if(!optional.isPresent()) {
            throw new ServiceException("Incorrect user id");
        }
        entityDTO.setPasswordHash(passwordEncoder.encode(entityDTO.getPasswordHash()));
        userDTORepository.save(entityDTO);

        return true;
    }

    @NotNull
    @Override
    @Transactional
    public Boolean persist(@NotNull final UserDTO entityDTO) {
        if (entityDTO.getId() == null || entityDTO.getId().isEmpty()) {
            entityDTO.setId(UUID.randomUUID().toString());
        }
        if (entityDTO.getUsername() == null || entityDTO.getUsername().isEmpty()) {
            throw new ServiceException("Incorrect Username");
        }
        if (entityDTO.getPasswordHash() == null || entityDTO.getPasswordHash().isEmpty()) {
            throw new ServiceException("Incorrect Password");
        }
        if (userDTORepository.findUserByUsername(entityDTO.getUsername()) != null) {
            throw new ServiceException("Username already exist");
        }
        entityDTO.setActive(true);
        entityDTO.setPasswordHash(passwordEncoder.encode(entityDTO.getPasswordHash()));
        userDTORepository.save(entityDTO);

        return true;
    }

    @NotNull
    @Override
    @Transactional
    public Boolean remove(@NotNull final String id) {
        @NotNull final Optional<User> optional = userRepository.findById(id);
        if(!optional.isPresent()) {
            throw new SecurityException("no such user");
        }
        @NotNull final User user = optional.get();
        userRepository.delete(user);
        return true;
    }
}
