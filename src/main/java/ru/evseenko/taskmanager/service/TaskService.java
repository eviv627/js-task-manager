package ru.evseenko.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.evseenko.taskmanager.api.repository.IDomainDTORepository;
import ru.evseenko.taskmanager.api.repository.ITaskDTORepository;
import ru.evseenko.taskmanager.api.repository.ITaskRepository;
import ru.evseenko.taskmanager.api.service.ITaskService;
import ru.evseenko.taskmanager.dto.TaskDTO;
import ru.evseenko.taskmanager.entity.Task;

import java.util.List;

@Service
public class TaskService extends AbstractDomainService<TaskDTO, Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    private final ITaskDTORepository taskDTORepository;

    @Autowired
    public TaskService(
            ITaskRepository taskRepository,
            ITaskDTORepository taskDTORepository
    ) {
        this.taskRepository = taskRepository;
        this.taskDTORepository = taskDTORepository;
    }

    @NotNull
    @Override
    ITaskRepository getDomainRepository() {
        return taskRepository;
    }

    @Override
    @NotNull IDomainDTORepository<TaskDTO> getDomainDTORepository() {
        return taskDTORepository;
    }

    @NotNull
    @Override
    @Transactional
    public List<TaskDTO> getTaskForProject(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final List<TaskDTO> entities =
                taskDTORepository.findTaskForProject(userId, projectId);

        return entities;
    }
}
