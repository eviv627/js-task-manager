package ru.evseenko.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.evseenko.taskmanager.api.repository.IDomainDTORepository;
import ru.evseenko.taskmanager.api.repository.IProjectDTORepository;
import ru.evseenko.taskmanager.api.repository.IProjectRepository;
import ru.evseenko.taskmanager.api.service.IProjectService;
import ru.evseenko.taskmanager.dto.ProjectDTO;
import ru.evseenko.taskmanager.entity.Project;

@Service
public class ProjectService extends AbstractDomainService<ProjectDTO, Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    private final IProjectDTORepository projectDTORepository;

    @Autowired
    public ProjectService(IProjectRepository projectRepository, IProjectDTORepository projectDTORepository) {
        this.projectRepository = projectRepository;
        this.projectDTORepository = projectDTORepository;
    }

    @NotNull
    @Override
    IProjectRepository getDomainRepository() {
        return projectRepository;
    }

    @Override
    @NotNull IDomainDTORepository<ProjectDTO> getDomainDTORepository() {
        return projectDTORepository;
    }

}
