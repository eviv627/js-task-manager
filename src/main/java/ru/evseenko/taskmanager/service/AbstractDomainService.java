package ru.evseenko.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.evseenko.taskmanager.api.entity.IDomainDTO;
import ru.evseenko.taskmanager.api.repository.IDomainDTORepository;
import ru.evseenko.taskmanager.api.repository.IDomainRepository;
import ru.evseenko.taskmanager.api.service.IDomainService;
import ru.evseenko.taskmanager.entity.Domain;
import ru.evseenko.taskmanager.entity.User;
import ru.evseenko.taskmanager.exception.ServiceException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public abstract class AbstractDomainService<T extends IDomainDTO, D extends Domain> implements IDomainService<T> {

    @NotNull
    abstract IDomainRepository<D> getDomainRepository();

    @NotNull
    abstract IDomainDTORepository<T> getDomainDTORepository();

    @NotNull
    @Override
    @Transactional
    public T get(@NotNull final String userId, @NotNull final String id){
        @NotNull final Optional<T> optional = getDomainDTORepository().findById(id);

        if(optional.isPresent()) {
            @NotNull final T domainDTO = optional.get();
            if (domainDTO.getUserId().equals(userId)) {
                return domainDTO;
            }
            throw new ServiceException("Incorrect User!");
        }
        throw new ServiceException("No such entity");
    }

    @NotNull
    @Override
    @Transactional
    public List<T> getAll(@NotNull String userId) {
        return getDomainDTORepository().findByUser(userId);
    }

    @NotNull
    @Override
    @Transactional
    public List<T> findByNamePart(@NotNull String userId, @NotNull String name) {
        return getDomainDTORepository().findByNamePart(userId, name);
    }

    @NotNull
    @Override
    @Transactional
    public List<T> findByDescriptionPart(@NotNull String userId, @NotNull String description){
        return getDomainDTORepository().findByDescriptionPart(userId, description);
    }

    @NotNull
    @Override
    @Transactional
    public Boolean update(@NotNull String userId, @NotNull T entityDTO) {
        @NotNull final IDomainDTORepository<T> repository = getDomainDTORepository();
        @NotNull final Optional<T> optional = repository.findById(entityDTO.getId());

        if(optional.isPresent()) {
            @NotNull final T domainDTO = optional.get();
            if (domainDTO.getUserId().equals(userId)) {
                repository.save(entityDTO);
                return true;
            }
            throw new ServiceException("Incorrect User!");
        }
        throw new ServiceException("No such entity");
    }

    @NotNull
    @Override
    @Transactional
    public Boolean persist(@NotNull String userId, @NotNull T entityDTO) {
        if (entityDTO.getId() == null || entityDTO.getId().isEmpty()) {
            entityDTO.setId(UUID.randomUUID().toString());
        }
        if (entityDTO.getUserId() == null || entityDTO.getUserId().isEmpty()) {
            entityDTO.setUserId(UUID.randomUUID().toString());
        }
        if (!userId.equals(entityDTO.getUserId())) {
            throw new ServiceException("Illegal access");
        }

        getDomainDTORepository().save(entityDTO);

        return true;
    }

    @NotNull
    @Override
    @Transactional
    public Boolean delete(@NotNull String userId, @NotNull T entityDTO) {
        @NotNull final IDomainRepository<D> repository = getDomainRepository();
        @NotNull final Optional<D> optional = repository.findById(entityDTO.getId());

        if(optional.isPresent()) {
            @NotNull final D domain = optional.get();

            @Nullable final User user = domain.getUser();
            if (user == null || user.getId() == null) {
                throw new ServiceException("Incorrect User!");
            }
            if (user.getId().equals(userId)) {
                repository.delete(domain);
                return true;
            }
            throw new ServiceException("Incorrect User!");
        }
        throw new ServiceException("No such entity");
    }

    @NotNull
    @Override
    @Transactional
    public Boolean deleteAll(@NotNull String userId) {
        @NotNull final List<D> list = getDomainRepository().findByUser(userId);
        for (D entity : list) {
            getDomainRepository().delete(entity);
        }
        return true;
    }
}
