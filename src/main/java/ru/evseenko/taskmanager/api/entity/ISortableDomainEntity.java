package ru.evseenko.taskmanager.api.entity;

import ru.evseenko.taskmanager.entity.Status;

import java.util.Date;

public interface ISortableDomainEntity {

    String getName();

    String getDescription();

    Date getStartDate();

    Date getEndDate();

    Date getCreateDate();

    Status getStatus();
}
