package ru.evseenko.taskmanager.api.entity;

public interface Identifiable {
    String getId();

    void setId(String id);
}
