package ru.evseenko.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.taskmanager.api.entity.IDomainDTO;

import java.util.List;

public interface IDomainService<T extends IDomainDTO> {

    @NotNull
    T get(@NotNull String userId, @NotNull String id);

    @NotNull
    List<T> getAll(@NotNull String userId);

    @NotNull
    List<T> findByNamePart(@NotNull String userId, @NotNull String name);

    @NotNull
    List<T> findByDescriptionPart(@NotNull String userId, @NotNull String description);

    @NotNull
    Boolean update(@NotNull String userId, @NotNull T entityDTO);

    @NotNull
    Boolean persist(@NotNull String userId, @NotNull T entityDTO);

    @NotNull
    Boolean delete(@NotNull String userId, @NotNull T entityDTO);

    @NotNull
    Boolean deleteAll(@NotNull String userId);
}
