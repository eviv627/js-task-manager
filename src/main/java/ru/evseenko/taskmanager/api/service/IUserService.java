package ru.evseenko.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.evseenko.taskmanager.dto.UserDTO;

public interface IUserService extends UserDetailsService {
    @Nullable
    UserDTO loadUserByUsername(@NotNull String username);

    @Nullable
    UserDTO get(@NotNull String id);

    @NotNull
    Boolean update(@NotNull UserDTO entityDTO);

    @NotNull
    Boolean persist(@NotNull UserDTO entityDTO);

    @NotNull
    Boolean remove(@NotNull String id);
}
