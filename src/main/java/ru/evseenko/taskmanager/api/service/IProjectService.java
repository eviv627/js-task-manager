package ru.evseenko.taskmanager.api.service;


import ru.evseenko.taskmanager.dto.ProjectDTO;

public interface IProjectService extends IDomainService<ProjectDTO> {
}
