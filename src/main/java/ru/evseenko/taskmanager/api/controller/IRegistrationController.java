package ru.evseenko.taskmanager.api.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.evseenko.taskmanager.dto.RegistrationRequest;
import ru.evseenko.taskmanager.dto.SimpleResponse;


public interface IRegistrationController {
    @PostMapping
    SimpleResponse registration(@RequestBody final RegistrationRequest user);
}
