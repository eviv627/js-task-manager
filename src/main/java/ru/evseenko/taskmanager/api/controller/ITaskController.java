package ru.evseenko.taskmanager.api.controller;

import ru.evseenko.taskmanager.dto.TaskDTO;

public interface ITaskController extends ICrudController<TaskDTO> {
}
