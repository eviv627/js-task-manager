package ru.evseenko.taskmanager.api.controller;


import ru.evseenko.taskmanager.dto.ProjectDTO;

import java.util.List;

public interface IProjectController extends ICrudController<ProjectDTO> {
}
