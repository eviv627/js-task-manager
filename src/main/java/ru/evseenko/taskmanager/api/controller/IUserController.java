package ru.evseenko.taskmanager.api.controller;

import ru.evseenko.taskmanager.dto.UserDTO;

public interface IUserController extends ICrudController<UserDTO> {
}
