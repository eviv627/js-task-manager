package ru.evseenko.taskmanager.api.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.evseenko.taskmanager.dto.AuthRequest;
import ru.evseenko.taskmanager.dto.SimpleResponse;


public interface IAuthenticationController {
    @PostMapping
    SimpleResponse login(@RequestBody final AuthRequest user);
}
