package ru.evseenko.taskmanager.api.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.evseenko.taskmanager.api.entity.Identifiable;
import ru.evseenko.taskmanager.dto.SimpleResponse;

import java.util.List;

public interface ICrudController<T extends Identifiable> {

    @GetMapping
    List<T> findAllEntities();

    @DeleteMapping
    SimpleResponse deleteAllEntities();

    @GetMapping(value = "/{id}")
    T findEntityById(@PathVariable(value = "id") final String id);

    @PostMapping
    SimpleResponse addEntity(@RequestBody final T entity);

    @PutMapping
    SimpleResponse updateEntity(@RequestBody final T entity);

    @DeleteMapping(value = "/{id}")
    SimpleResponse deleteEntity(@PathVariable(value = "id") final String id);
}
