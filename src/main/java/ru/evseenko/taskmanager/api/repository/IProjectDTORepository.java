package ru.evseenko.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.evseenko.taskmanager.dto.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectDTORepository extends JpaRepository<ProjectDTO, String>, IDomainDTORepository<ProjectDTO> {

    @Cacheable("findByUser")
    @NotNull List<ProjectDTO> findByUser(@NotNull @Param("userId") String userId);

    @Cacheable("findByNamePart")
    @NotNull List<ProjectDTO> findByNamePart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Cacheable("findByDescriptionPart")
    @NotNull List<ProjectDTO> findByDescriptionPart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("description") String description
    );
}

