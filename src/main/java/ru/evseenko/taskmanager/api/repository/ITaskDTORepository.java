package ru.evseenko.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.evseenko.taskmanager.dto.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDTORepository extends JpaRepository<TaskDTO, String>, IDomainDTORepository<TaskDTO> {

    @NotNull List<TaskDTO> findByUser(@NotNull @Param("userId") String userId);

    @NotNull List<TaskDTO> findByNamePart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @NotNull List<TaskDTO> findByDescriptionPart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("description") String description
    );

    @NotNull List<TaskDTO> findTaskForProject(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );
}
