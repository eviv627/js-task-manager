package ru.evseenko.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.evseenko.taskmanager.api.entity.IDomainDTO;

import java.util.List;

@NoRepositoryBean
public interface IDomainDTORepository<T extends IDomainDTO> extends JpaRepository<T, String> {

    @NotNull List<T> findByUser(@NotNull String userId);

    @NotNull List<T> findByNamePart(@NotNull String userId, @NotNull String name);

    @NotNull List<T> findByDescriptionPart(@NotNull String userId, @NotNull String description);
}
