package ru.evseenko.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.evseenko.taskmanager.dto.UserDTO;

@Repository
public interface IUserDTORepository extends JpaRepository<UserDTO, String> {

    @Nullable UserDTO findUserByUsername(@NotNull @Param("username") final String username);
}
