package ru.evseenko.taskmanager.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import ru.evseenko.taskmanager.api.entity.ISortableDomainEntity;

import java.util.Comparator;

@Getter
public enum Sort {

    NAME("name", Comparator.comparing(ISortableDomainEntity::getName)),

    READY("status",
            Comparator.comparing(ISortableDomainEntity::getStatus)
                    .thenComparing(ISortableDomainEntity::getName)
    ),

    CREATE("create", Comparator.comparing(ISortableDomainEntity::getCreateDate)),

    END_DATE("end date", Comparator.comparing(ISortableDomainEntity::getEndDate)),

    START_DATE("start date", Comparator.comparing(ISortableDomainEntity::getStartDate));

    private final String name;

    private final Comparator<ISortableDomainEntity> comparator;

    Sort(String name, Comparator<ISortableDomainEntity> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

    @JsonCreator
    public static Sort forValue(String value) {
        return Sort.valueOf(value);
    }

    @JsonValue
    public static String toValue(Sort sort) {
        return sort.getName();
    }

}
