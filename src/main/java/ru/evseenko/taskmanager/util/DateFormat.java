package ru.evseenko.taskmanager.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateFormat {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public static String format(final Date date) {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("MSK"));
        return DATE_FORMAT.format(date);
    }
}
