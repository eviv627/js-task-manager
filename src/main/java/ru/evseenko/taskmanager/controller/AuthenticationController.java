package ru.evseenko.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.evseenko.taskmanager.api.controller.IAuthenticationController;
import ru.evseenko.taskmanager.api.service.IUserService;
import ru.evseenko.taskmanager.dto.AuthRequest;
import ru.evseenko.taskmanager.dto.SimpleResponse;
import ru.evseenko.taskmanager.dto.UserDTO;
import ru.evseenko.taskmanager.exception.ServiceException;

@RestController
@RequestMapping(value = "/api/login")
public class AuthenticationController implements IAuthenticationController {
    private final AuthenticationManager authenticationManager;

    private final IUserService userService;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, IUserService userService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    @Override
    public SimpleResponse login(AuthRequest userAuth) {
        @NotNull final String username = userAuth.getUsername();
        @NotNull final String password = userAuth.getPassword();
        @Nullable final UserDTO user = userService.loadUserByUsername(username);

        if (user == null) {
            throw new ServiceException("No Such User");
        }

        final Authentication authenticate = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password)
        );

        if (authenticate.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authenticate);
        }

        @NotNull final SimpleResponse resp = new SimpleResponse();
        resp.setSuccess(true);
        if (user.getId() != null) {
            resp.setValue(user.getId());
        }
        return resp;
    }
}
