package ru.evseenko.taskmanager.controller;

import ru.evseenko.taskmanager.api.controller.IUserController;
import ru.evseenko.taskmanager.dto.SimpleResponse;
import ru.evseenko.taskmanager.dto.UserDTO;

import java.util.List;

public class UserController implements IUserController {

    @Override
    public List<UserDTO> findAllEntities() {
        return null;
    }

    @Override
    public SimpleResponse deleteAllEntities() {
        return null;
    }

    @Override
    public UserDTO findEntityById(String id) {
        return null;
    }

    @Override
    public SimpleResponse addEntity(UserDTO entity) {
        return null;
    }

    @Override
    public SimpleResponse updateEntity(UserDTO entity) {
        return null;
    }

    @Override
    public SimpleResponse deleteEntity(String id) {
        return null;
    }
}
