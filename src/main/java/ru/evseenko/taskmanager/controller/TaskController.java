package ru.evseenko.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.evseenko.taskmanager.api.controller.ITaskController;
import ru.evseenko.taskmanager.api.service.ITaskService;
import ru.evseenko.taskmanager.dto.TaskDTO;
import ru.evseenko.taskmanager.dto.SimpleResponse;
import ru.evseenko.taskmanager.util.Controller;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/task")
public class TaskController implements ITaskController {

    private final ITaskService taskService;

    @Autowired
    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }


    @Override
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<TaskDTO> findAllEntities() {
        return taskService.getAll(Controller.getUserId());
    }

    @Override
    @DeleteMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public SimpleResponse deleteAllEntities() {
        @NotNull final SimpleResponse response = new SimpleResponse();
        try {
            taskService.deleteAll(Controller.getUserId());
            response.setSuccess(true);
        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public TaskDTO findEntityById(String id) {
        return taskService.get(Controller.getUserId(), id);
    }

    @Override
    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public SimpleResponse addEntity(@NotNull final TaskDTO task) {
        @NotNull final SimpleResponse response = new SimpleResponse();
        try {
            if (task.getId().isEmpty()) {
                task.setId(UUID.randomUUID().toString());
                response.setValue(task.getId());
            }
            taskService.persist(Controller.getUserId(), task);
            response.setSuccess(true);
        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    @PutMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public SimpleResponse updateEntity(@NotNull final TaskDTO task) {
        @NotNull final SimpleResponse response = new SimpleResponse();
        try {
            taskService.update(Controller.getUserId(), task);
            response.setSuccess(true);
        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    @DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public SimpleResponse deleteEntity(@PathVariable(value = "id") @NotNull final String id) {
        @NotNull final SimpleResponse response = new SimpleResponse();
        try {
            final String userId = Controller.getUserId();
            taskService.delete(userId, taskService.get(userId, id));
            response.setSuccess(true);
        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }
}
