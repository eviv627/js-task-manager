package ru.evseenko.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.evseenko.taskmanager.api.controller.IProjectController;
import ru.evseenko.taskmanager.api.service.IProjectService;
import ru.evseenko.taskmanager.dto.ProjectDTO;
import ru.evseenko.taskmanager.dto.SimpleResponse;
import ru.evseenko.taskmanager.util.Controller;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/project")
public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    @Autowired
    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }


    @Override
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<ProjectDTO> findAllEntities() {
        return projectService.getAll(Controller.getUserId());
    }

    @Override
    @DeleteMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public SimpleResponse deleteAllEntities() {
        @NotNull final SimpleResponse response = new SimpleResponse();
        try {
            projectService.deleteAll(Controller.getUserId());
            response.setSuccess(true);
        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ProjectDTO findEntityById(String id) {
        return projectService.get(Controller.getUserId(), id);
    }

    @Override
    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public SimpleResponse addEntity(@NotNull final ProjectDTO project) {
        @NotNull final SimpleResponse response = new SimpleResponse();
        try {
            if (project.getId().isEmpty()) {
                project.setId(UUID.randomUUID().toString());
                response.setValue(project.getId());
            }
            projectService.persist(Controller.getUserId(), project);
            response.setSuccess(true);
        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    @PutMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public SimpleResponse updateEntity(@NotNull final ProjectDTO project) {
        @NotNull final SimpleResponse response = new SimpleResponse();
        try {
            projectService.update(Controller.getUserId(), project);
            response.setSuccess(true);
        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    @DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public SimpleResponse deleteEntity(@PathVariable(value = "id") @NotNull final String id) {
        @NotNull final SimpleResponse response = new SimpleResponse();
        try {
            final String userId = Controller.getUserId();
            projectService.delete(userId, projectService.get(userId, id));
            response.setSuccess(true);
        } catch (Exception e) {
            response.setSuccess(false);
        }
        return response;
    }
}
