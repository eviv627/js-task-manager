package ru.evseenko.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.evseenko.taskmanager.api.service.IUserService;
import ru.evseenko.taskmanager.dto.UserDTO;
import ru.evseenko.taskmanager.entity.Role;

import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Autowired
    private IUserService userService;

    private UserDTO user;

    @Before
    public void setUpTest() {
        user = new UserDTO();
        user.setId(UUID.randomUUID().toString());
        user.setUsername("LOGIN");
        user.setPasswordHash("password hash");
        user.setRole(Role.ADMIN);
        userService.persist(user);
    }

    @After
    public void tearDownTest() {
        userService.remove(user.getId());
    }

    @Test
    public void createUserTest() {
        @Nullable final UserDTO actualUser = userService.get(user.getId());
        Assert.assertEquals(user.toString(), actualUser.toString());
    }

    @Test
    public void updateUserTest() {
        user.setRole(Role.USER);
        user.setUsername("LOG");
        user.setPasswordHash("HASH");
        userService.update(user);

        @Nullable final UserDTO actualUser = userService.get(user.getId());
        assert actualUser != null;
        Assert.assertEquals(user.toString(), actualUser.toString());
    }

    @Test
    public void getByLoginUserTest() {
        @Nullable final UserDTO actualUser = userService.loadUserByUsername(user.getUsername());
        assert actualUser != null;
        Assert.assertEquals(user.toString(), actualUser.toString());
    }
}
