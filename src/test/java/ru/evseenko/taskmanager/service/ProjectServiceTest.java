package ru.evseenko.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.evseenko.taskmanager.api.service.IProjectService;
import ru.evseenko.taskmanager.api.service.ITaskService;
import ru.evseenko.taskmanager.api.service.IUserService;
import ru.evseenko.taskmanager.dto.ProjectDTO;
import ru.evseenko.taskmanager.dto.TaskDTO;
import ru.evseenko.taskmanager.dto.UserDTO;
import ru.evseenko.taskmanager.util.Sort;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProjectServiceTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ITaskService taskService;

    private String testUserId;

    @Before
    public void init() {
        UserDTO testUser = new UserDTO();
        testUser.setId(UUID.randomUUID().toString());
        testUser.setUsername(generateUserName());
        testUser.setPasswordHash("hash");
        userService.persist(testUser);

        testUserId = testUser.getId();
    }

    @After
    public void removeUser() {
        userService.remove(testUserId);
    }

    @Test
    public void projectCreateTest() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        project.setName("project1");
        project.setUserId(testUserId);
        project.setDescription("desc");
        project.setCreateDate(new Date());
        project.setStartDate(new Date());
        project.setEndDate(new Date());

        projectService.persist(project.getUserId(), project);

        @NotNull final ProjectDTO actualProject =
                projectService.get(project.getUserId(), project.getId());

        Assert.assertEquals(project.toString(), actualProject.toString());
    }

    @Test
    public void projectUpdateTest() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        project.setName("project1");
        project.setUserId(testUserId);
        project.setDescription("desc");
        project.setCreateDate(new Date());
        project.setStartDate(new Date());
        project.setEndDate(new Date());

        projectService.persist(project.getUserId(), project);

        project.setName("upd_project1");
        project.setDescription("upd_desc");
        project.setCreateDate(new Date());
        project.setStartDate(new Date());
        project.setEndDate(new Date());

        projectService.update(project.getUserId(), project);

        @NotNull final ProjectDTO actualProject =
                projectService.get(project.getUserId(), project.getId());

        Assert.assertEquals(project.toString(), actualProject.toString());
    }

    @Test
    public void projectDeleteAllTest() {
        final int projectsNum = 20;
        final int tasksNum = 20;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            final ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setUserId(testUserId);
            project.setDescription("desc");
            project.setCreateDate(new Date());
            projectService.persist(testUserId, project);
            listProjectDTO.add(project);
        }

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("project" + i);
            task.setUserId(testUserId);
            task.setDescription("desc");
            task.setCreateDate(new Date());
            task.setProjectId(listProjectDTO.get(i).getId());
            taskService.persist(testUserId, task);
        }

        projectService.deleteAll(testUserId);

        Assert.assertEquals(new ArrayList<>(), taskService.getAll(testUserId));
        Assert.assertEquals(new ArrayList<>(), projectService.getAll(testUserId));
    }

    @Test
    public void projectDeleteTaskTest() {
        final int projectsNum = 20;
        final int tasksNum = 200;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();
        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            final ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setUserId(testUserId);
            project.setDescription("desc");
            project.setCreateDate(new Date());
            projectService.persist(testUserId, project);
            listProjectDTO.add(project);
        }

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("project" + i);
            task.setUserId(testUserId);
            task.setDescription("desc");
            task.setCreateDate(new Date());
            task.setProjectId(listProjectDTO.get(i/10).getId());
            taskService.persist(testUserId, task);
            listTaskDTO.add(task);
        }

        projectService.delete(testUserId, listProjectDTO.remove(0));

        int i = 0;
        Iterator<TaskDTO> iteratorTaskDTO = listTaskDTO.iterator();
        while (i < 10) {
            if (iteratorTaskDTO.hasNext()) {
                iteratorTaskDTO.next();
                iteratorTaskDTO.remove();
            }
            i++;
        }

        @NotNull final List<ProjectDTO> actualProjects = projectService.getAll(testUserId);
        actualProjects.sort(Sort.NAME.getComparator());
        listProjectDTO.sort(Sort.NAME.getComparator());
        @NotNull final List<TaskDTO> actualTasks = taskService.getAll(testUserId);
        actualTasks.sort(Sort.NAME.getComparator());
        listTaskDTO.sort(Sort.NAME.getComparator());

        Assert.assertEquals(listProjectDTO.toString(), actualProjects.toString());
        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @Test
    public void projectByNameTest() {
        final int projectsNum = 20;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            final ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setUserId(testUserId);
            project.setDescription("desc");
            project.setCreateDate(new Date());
            projectService.persist(testUserId, project);

            if (project.getName().contains("project1")) {
                listProjectDTO.add(project);
            }
        }

        @NotNull final List<ProjectDTO> actualProjects =
                projectService.findByNamePart(testUserId, "project1");
        actualProjects.sort(Sort.NAME.getComparator());
        listProjectDTO.sort(Sort.NAME.getComparator());

        Assert.assertEquals(listProjectDTO.toString(), actualProjects.toString());
    }

    @Test
    public void  projectByDescriptionTest() {
        final int projectsNum = 20;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            final ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setUserId(testUserId);
            project.setDescription("desc" + i);
            project.setCreateDate(new Date());
            projectService.persist(testUserId, project);

            if (project.getDescription().contains("desc1")) {
                listProjectDTO.add(project);
            }
        }

        @NotNull final List<ProjectDTO> actualProjects =
                projectService.findByDescriptionPart(testUserId, "desc1");
        actualProjects.sort(Sort.NAME.getComparator());
        listProjectDTO.sort(Sort.NAME.getComparator());

        Assert.assertEquals(listProjectDTO.toString(), actualProjects.toString());
    }

    @NotNull
    private String generateUserName() {
        @NotNull final Random random = new Random();
        return "TestServiceUser" + random.nextInt(999999);
    }
}
