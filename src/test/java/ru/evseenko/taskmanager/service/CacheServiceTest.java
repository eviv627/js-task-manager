package ru.evseenko.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.test.context.junit4.SpringRunner;
import ru.evseenko.taskmanager.api.service.IProjectService;
import ru.evseenko.taskmanager.api.service.ITaskService;
import ru.evseenko.taskmanager.api.service.IUserService;
import ru.evseenko.taskmanager.dto.ProjectDTO;
import ru.evseenko.taskmanager.dto.TaskDTO;
import ru.evseenko.taskmanager.dto.UserDTO;
import ru.evseenko.taskmanager.util.Sort;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CacheServiceTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ITaskService taskService;

    private String testUserId;

    @Before
    public void init() {
        UserDTO testUser = new UserDTO();
        testUser.setId(UUID.randomUUID().toString());
        testUser.setUsername(generateUserName());
        testUser.setPasswordHash("hash");
        userService.persist(testUser);

        testUserId = testUser.getId();
    }

    @After
    public void removeUser() {
        userService.remove(testUserId);
    }


    @Test
    public void simpleCacheTest() {
        final int tasksNum = 200;

        final ProjectDTO project = new ProjectDTO();

        project.setId(UUID.randomUUID().toString());
        project.setName("project");
        project.setUserId(testUserId);
        project.setDescription("desc");
        project.setCreateDate(new Date());
        projectService.persist(testUserId, project);

        ProjectDTO actualProject = projectService.get(testUserId, project.getId());
        actualProject = projectService.get(testUserId, project.getId());
        assert actualProject.getId() != null;
        actualProject = projectService.get(testUserId, project.getId());
        assert actualProject.getId() != null;
        actualProject = projectService.get(testUserId, project.getId());
        assert actualProject.getId() != null;
        actualProject = projectService.get(testUserId, project.getId());
        assert actualProject.getId() != null;
        actualProject = projectService.get(testUserId, project.getId());
        assert actualProject.getId() != null;
        actualProject = projectService.get(testUserId, project.getId());
        assert actualProject.getId() != null;
        actualProject = projectService.get(testUserId, project.getId());
        assert actualProject.getId() != null;
        actualProject = projectService.get(testUserId, project.getId());

        Assert.assertEquals(project.toString(), actualProject.toString());
    }


    @Test
    public void taskForProjectCacheTest() {
        final int tasksNum = 200;

        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();
        final ProjectDTO project = new ProjectDTO();

        project.setId(UUID.randomUUID().toString());
        project.setName("project");
        project.setUserId(testUserId);
        project.setDescription("desc");
        project.setCreateDate(new Date());
        projectService.persist(testUserId, project);

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("project" + i);
            task.setUserId(testUserId);
            task.setDescription("desc");
            task.setCreateDate(new Date());
            task.setProjectId(((i % 10) > 5) ? project.getId() : "");
            taskService.persist(testUserId, task);

            if (task.getProjectId().isEmpty()) {
                listTaskDTO.add(task);
            }
        }

        @NotNull final List<TaskDTO> cachingTasks = taskService.getAll(testUserId); //Caching requests
        assert !cachingTasks.isEmpty();

        @NotNull final TaskDTO cachingTask = taskService.get(testUserId, listTaskDTO.get(0).getId());
        assert !cachingTask.getId().isEmpty();

        projectService.delete(testUserId, project);



        @NotNull final List<TaskDTO> actualTasks = taskService.getAll(testUserId);
        actualTasks.sort(Sort.NAME.getComparator());
        listTaskDTO.sort(Sort.NAME.getComparator());

        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @NotNull
    private String generateUserName() {
        @NotNull final Random random = new Random();
        return "TestServiceUser" + random.nextInt(999999);
    }
}
